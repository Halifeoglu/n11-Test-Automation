import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static configuration.Configuration.WAIT_LONG;
import static configuration.Configuration.WAIT_MEDIUM;

public abstract class BasePage {

    WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }



    public void clickTo(By by){
        WebDriverWait wait =new WebDriverWait(driver,WAIT_MEDIUM);
        wait.until(ExpectedConditions.elementToBeClickable(by)).click();
    }

    public  void scrollTo(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
    }

    public void clickandType(By by , String string){
        WebDriverWait wait =new WebDriverWait(driver,WAIT_MEDIUM);
        WebElement item=wait.until(ExpectedConditions.elementToBeClickable(by));
        item.click();
        item.sendKeys(string);
    }

    public WebElement waitForVisibility(By by) {
        WebDriverWait wait = new WebDriverWait(driver, WAIT_LONG);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public Boolean isElementDisplayed(By by) {
        WebDriverWait wait = new WebDriverWait(driver,WAIT_MEDIUM);
        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (NoSuchElementException e) {
            System.out.println(e);
            return false;
        }
        return true;
    }

}
