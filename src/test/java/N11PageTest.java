import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.assertThat;


public class N11PageTest extends BaseTest{

    @Test
    public void N11Test() {
        N11Page n11Page=new N11Page(driver);
        assertThat("When user opened the n11 site , must see correct page", n11Page.homePageControl("https://www.n11.com/"));

        UserPool.userInformation().standartLogin(n11Page);
        assertThat("When user was logIn  , must see own name", n11Page.userControl("Samet Halifeoglu"));

        n11Page.searchProducts("Samsung");
        assertThat("When user searched Samsung's products  , must see Samsung results", n11Page.wantedProductControl("Samsung"));

        n11Page.open2ndPage();
        assertThat("When user clicked 2nd Page  , must see correct results", n11Page.secondPageControl("pg=2"));

        n11Page.thirthProductAddFavorite();
        assertThat("When user opened favorite Page  , must see equals product", n11Page.productMatch());

        n11Page.deleteProductInFavoritePage();
        assertThat("When the user deletes the product from the favorites, should see the message.", n11Page.messageControl("Ürününüz listeden silindi."));

        n11Page.complete();
        assertThat("When user deleted product from favorite Page , must see empty favorite page ", n11Page.emptyFavoritePageControl());

    }
}
